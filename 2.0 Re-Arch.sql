-- This piece is to get all Admins at Instances

Select 
`account id`
,`account name`
,`user name`
,`email address`
,`csm_name`
,`csm_email`
,`ae_name`
,`ae_email`
,`user id`
-- ,"Admin" as why_added
from `analytics_user_list_all`
where `user status` = 'Current'
and `user type` = 'Customer'
and lower(`user role`) like '%admin%'
and `freemium_status` = 'paid' -- will need to remove this when we take in Standard Accounts
and `is_primary` = 'true'
and `acsm_program` = 'Yes' -- This allows only those who have been selected in SFDC as part of the program to get emails. 
and `account status` = 'Active'


-- This piece is get all majordomos

Union 

SELECT
`account id`
,`account name`
,`user name`
,`email address`
,`csm_name`
,`csm_email`
,`ae_name`
,`ae_email`
,`user id`
-- ,'MajorDomo' as why_added
from `analytics_user_list_all`
where `user status` = 'Current'
and `user type` = 'Customer'
and `freemium_status` = 'paid' -- will need to remove this when we take in Standard Accounts
and `is_primary` = 'true'
and `acsm_program` = 'Yes'
and `md` is not null -- this line specifically is what predicts if someone is a majordomo. 
and `account status` = 'Active'

-- This section gets anyone that fits into one of our Command of the message personas

UNION -- Using a union to remove any duplicates

SELECT
`Account_Id`
,`Account_Name`
,`User_Name`
,`email address`
,`CSM_NAME`
,`csm_email`
,`ae_name`
,`ae_email`
,`User_Id`

from `acsm_abm_account_conquest`
where FLAG <> 'None'
and `Individual_Origin` = 'User_In_Instance'

and `acsm_program` = 'Yes'

--------------------------------------

-- This table prior to being brought into this dataflow only has data for Dustin Parkinson, Max Isley, Guy Welker, and Curtis Phillip

Select *
,datediff(CURRENT_DATE,`Date_Time`) as Test
from `acsm_outlook_emails_splitter`
Where
datediff(CURRENT_DATE,`Date_Time`) < 7
and `To_Email_Address` not like '%@domo.com'

-------------------------------------
-- if ACSM program opts to not use Domo Campaigns, then this step is not needed. 

-- Checks for opt outs in Salesforce. 
Select 
distinct(lower(`c.email`)) as opted_out_email
from `kaizen_contacts`
where `c.Opted Out of Email` = 'true'
and `c.email` is not null

Union -- using union to remove any duplicated between the two datasets. 

-- checks for opt outs in Eloqua. 
Select
distinct(lower(`EmailAddress`)) as opted_out_email 
from `eloqua_unsubscribes`
where `EmailAddress` is not null
-- we don't need additional filtering here becasue this eloqua dataset only has data for unsubscribed emails. 

-----------------------------------------

--This is for High Touch targeting from Kaizen. (Not For ACSM)

Select 
`a.ID` as "account id"
,`a.Name` as "account name"
,`c.Name` as "user name"
,`c.Email` as "email address"
,`a.CSM Name` as csm_name
,`a.CSM Email` as csm_email
,`a.Account Owner` as ae_name
,`a.Account Owner Email` as ae_email
from `kaizen_contacts`
where 
`a.Account Status` = 'Active'
and `a.Customer Type` in ('Domo Customer', 'Domo/Legacy Customer')
and `c.Contact Role` in (
'Project Lead'
,'Data/IT Contact'
,'Executive Sponsor'
,'Project/Data Lead'
,'Executive/Project Lead'
,'General User'
,'MajorDomo'
)

------------------------------------------

-- Also for HT (Not ACSM)
Select 
`account id`
,`email address`
,`user id` 
from `analytics_user_list_all`
where `user status` = 'Current'
and `user type` = 'Customer'
and `freemium_status` = 'paid'
and `is_primary` = 'true'
and `account status` = 'Active'

--------------------------------------------

-- Joining HT tables together to create HT Master
Select 
ht1.*
,ht2.`user id`
from `ht_aug_prep` ht1
left join `ht_aug_prep_2` ht2
on ht1.`account id` = ht2.`account id`
and ht1.`email address` = ht2.`email address`

-------------------------------------------

-- This removes all opt outs and any others that the CSM wants removed from the send list. Combines user list. 

select *
from `user_list`
where `email address` not in (Select * from `all_email_opt_outs`)
and `email address` not in (Select `User_Email` from `acsm_include_exclude_list` where `Include_Exclude` = 'Exclude')
and `email address` not in (Select `To_Email_Address` from `outlook_contact`)

Union -- using union to ensure no duplicates in the list

-- this allows for the CSM to add any users they want to the list at their discretion. 
SELECT 
b.`Account_ID` as "account id"
,b.`Account_Name`as "account name"
,b.`User_Name` as "user name"
,b.`User_Email` as "email address"
,max(`csm_name`) as csm_name
,max(`csm_email`) as csm_email
,max(`ae_name`) as ae_name
,max(`ae_email`) ae_email
,max(`user id`) as 'user id'
-- ,"CSM_Manual_Add" as `why_added`
from `analytics_user_list_all` a
inner join 
`acsm_include_exclude_list` b 
on a.`account id` = b.`Account_ID`
and lower(trim(a.`email address`)) = lower(trim(b.`User_Email`))
where `Include_Exclude` = 'Include'
group by 
b.`Account_ID`
,b.`Account_Name`
,b.`User_Name`
,b.`User_Email`

UNION

Select *
from `high_touch_aug`


---------------------------------

--Create Final User List

Select 
max(`account id`) "account id"
,max(`account name`) "account name"
,max(`user name`) "user name"
,`email address` "email address"
,max(`csm_name`) csm_name
,max(`csm_email`) csm_email
,max(`ae_name`) ae_name
,max(`ae_email`) ae_email
,`user id`
from `combine_user_list`
group by `user id`
,`email address`

--------------------------------

--Track history of campaign emails. 
select    
emailaddress
,max(case when assetname = '08.19_ACSM_US_NewUser' then activitydate end) as NewUser_last_date
,max(case when assetname = '08.19_ACSM_US_7_Samurai' then activitydate end) as 7_Samurai_last_date
,max(case when assetname = '08.19_ACSM_US_Flywheel' then activitydate end) as FlyWheel_last_date
,max(case when assetname = '08.19_ACSM_US_Escalation' then activitydate end) as Escalation_last_date
,max(case when assetname = '08.19_ACSM_US_PersonalHealth' then activitydate end) as Personal_Health_last_date
,max(case when assetname = '08.19_ACSM_US_Adoption' then activitydate end) as Adoption_last_date
,max(case when assetname = '08.19_ACSM_US_Instance_Governance' then activitydate end) as Instance_Governance_last_date
,max(case when assetname = '08.19_ACSM_US_BiAnnualReview' then activitydate end) as Bi_Annual_Review_last_date    
,max(case when assetname like  '%ConstantContact%' then activitydate end) as constant_contact_last_date
,max(case when assetname like '%ACSM%' then activitydate end) as any_acsm_last_date

from markops_eloqua_email

where activitytype = 'EmailSend' and assetname like '%ACSM%'

group by
emailaddress

--------------------------------

--New user intro email qualification.
Select 
`user id`
,1 as new_user_intro_candidate
-- ,datediff(CURRENT_DATE,`created date`) as Days_Since_Creation
from `analytics_user_list_all`
where datediff(CURRENT_DATE,`created date`) <=14
--------------------------------

--Isolate last 4 weeks
Select 
distinct(snapshot_week)
FROM `analytics_activity_health_index_all`
order by snapshot_week DESC
LIMIT 4

--------------------------------
--Isolate last 4 weeks W/ Row # for future id

SELECT a.*
,@row := @row + 1 as row_num
FROM `last_4_weeks_l4` a,
(SELECT @row := 0) r;
--------------------------------

--Identify health score (P_renew) four week backs until now. 
Select 
`account id`
,`account name`
, csm_name
, avg(case when snapshot_week = (SELECT snapshot_week from last_4_with_rownum where row_num = 1) then p_renew end) as 1_week_prior
, avg(case when snapshot_week = (SELECT snapshot_week from last_4_with_rownum where row_num = 2) then p_renew end) as 2_week_prior
, avg(case when snapshot_week = (SELECT snapshot_week from last_4_with_rownum where row_num = 3) then p_renew end) as 3_week_prior
, avg(case when snapshot_week = (SELECT snapshot_week from last_4_with_rownum where row_num = 4) then p_renew end) as 4_week_prior
FROM `analytics_activity_health_index_all`
where at_risk_cat <> 'Less than 6 months of tenure'
and `account status` = 'Active'
group by 
`account id`
,`account name`
, csm_name
--------------------------------
--------------------------------
--Categorize health scores with weeks. <50% is unhealthy

Select *
,(case when 1_week_prior <.5 then 1 else 0 END
+
case when 2_week_prior <.5 then 1 else 0 END
+
case when 3_week_prior <.5 then 1 else 0 END
+
case when 4_week_prior <.5 then 1 else 0 END) as health_test

from health_score
--------------------------------

 --THis is just identifying different event dates on the account for future use. 
select 
`account id`
,max(`last login`) as Admin_Last_Login
,min(`instance_created_date`) as `instance_created_date`
,min(`customer signed date`) as 'customer signed date'
from `analytics_user_list_all`
where`user type` = 'Customer'
and `user status` = 'Current'
and `account status` = 'Active'
and `user role` = 'Admin'
group by `account id`
 

--------------------------------
--Looking for escalation candidates. 
--Criteria: Low health within last 4 weeks, admin hasnt logged in for > 14 days, instance was created >= 180 days ago. 

Select 
a.`account id`
,1 as escalation_candidate
from `health_test` a

left join `account_event_dates` b
on a.`account id` = b.`account id`

where a.`health_test` = 4
and datediff(CURRENT_DATE, b.`Admin_Last_Login`) > 14
and datediff(CURRENT_DATE, b.`instance_created_date`) >= 180
and datediff(CURRENT_DATE, b.`customer signed date`) >= 180

--------------------------------
-- Users

Select 
`account id`
,COUNT(`user id`) as Total_Users
from `analytics_user_list_all`
where `user status` = 'Current'
and `user type` = 'Customer'
and `account status` = 'Active'                
group by `account id`

--------------------------------
-- Cards
Select 
`account id`
,count(`kpi_id`) as Total_Cards
from `analytics_card_info_all`
where `creator_user_type` in ('Consulting Partner', 'Customer', 'Domo')
and `type` in ('badge', 'kpi', 'table', 'Text')
group by `account id`
--------------------------------
-- Rows
Select 
`account id`
,sum(`rows`) as Total_Rows
from `analytics_datasources_all`
where `responsible_user_id` <> 99
and lower(`data_source_type`) <> 'datafusion'
group by 
`account id`
--------------------------------
--Average of each category above. 

select *
from 
(
Select round(AVG(`total_users`),0) as AVG_Users
from `users`
) a,

(
Select 
round(AVG(`total_cards`),0) as AVG_Cards
from 
`cards`
) b,

(
Select 
round(AVG(`Total_Rows`),0) as AVG_Rows
from
`rows`
) c
--------------------------------

 --Here we are IDing stale users 

SELECT
a.`account id`
,a.`account name`
,`user name`
,DATEDIFF(CURRENT_DATE(),ifnull(`last login`,`created date`)) days_since_last_interaction

,@rank:=  IF(@prev = a.`Account Id`,@rank + 1,1) AS `Rank`

,@prev:= a.`Account Id`

FROM  `analytics_user_list_all` a
,(SELECT @rank:=0, @prev:="") b

where `user status` = 'Current'
and `user type` = 'Customer'
and `account status` = 'Active'
and lower(`user role`) <> 'social'
and DATEDIFF(CURRENT_DATE(),ifnull(`last login`,`created date`)) >= 90 -- after 90 days, you should kick them out of the instance. 

ORDER by a.`Account Id`, DATEDIFF(CURRENT_DATE(),ifnull(`last login`,`created date`)) desc

--------------------------------
--Stale user Limit

SELECT
a.`account id`
,a.`last_updated_date`
,a.`data_source_name`
,a.`created`
,DATEDIFF(CURRENT_DATE(),ifnull(`last_updated_date`,`created`)) as Days_Since_Interaction

,@rank:=  IF(@prev = a.`Account Id`,@rank + 1,1) AS `Rank`

,@prev:= a.`Account Id`

FROM  `analytics_datasources_all` a
,(SELECT @rank:=0, @prev:="") b

Where DATEDIFF(CURRENT_DATE(),ifnull(`last_updated_date`,`created`)) >= 90 -- after 90 days, you should kick them out of the instance. 

ORDER by a.`Account Id`, DATEDIFF(CURRENT_DATE(),ifnull(`last_updated_date`,`created`)) desc
--------------------------------
--Stale Card Limit

Select 
`account id`
,max(case when `Rank` = 1 then concat(`data_source_name`, " - ", `Days_Since_Interaction`, " days since interaction") end) as stale_dataset_1
,max(case when `Rank` = 2 then concat(`data_source_name`, " - ", `Days_Since_Interaction`, " days since interaction") end) as stale_dataset_2
,max(case when `Rank` = 3 then concat(`data_source_name`, " - ", `Days_Since_Interaction`, " days since interaction") end) as stale_dataset_3
,max(case when `Rank` = 4 then concat(`data_source_name`, " - ", `Days_Since_Interaction`, " days since interaction") end) as stale_dataset_4
,max(case when `Rank` = 5 then concat(`data_source_name`, " - ", `Days_Since_Interaction`, " days since interaction") end) as stale_dataset_5
from `stale_data_rank`
where `Rank` in (1,2,3,4,5)
group by 
`account id`




--------------------------------
SELECT
a.`account id`
,a.`created`
,a.`kpi_title`
,a.`last_view`
,DATEDIFF(CURRENT_DATE(),ifnull(`last_view`,`created`)) as Days_Since_Interaction

,@rank:=  IF(@prev = a.`Account Id`,@rank + 1,1) AS `Rank`

,@prev:= a.`Account Id`

FROM `analytics_card_info_all` a
,(SELECT @rank:=0, @prev:="") b

where `creator_user_type` in ('Consulting Partner', 'Customer', 'Domo')
and `type` in ('badge', 'kpi', 'table', 'Text')
and DATEDIFF(CURRENT_DATE(),ifnull(`last_view`,`created`)) >= 90 -- after 90 days, you should kick them out of the instance. 

ORDER by a.`Account Id`, DATEDIFF(CURRENT_DATE(),ifnull(`last_view`,`created`)) desc

--------------------------------

--Identifying # of datasets created in last 120 days (4 month period)

Select 
`responsible_user_id`
,sum(case when datediff(CURRENT_DATE(),`created`) < 120 then 1 else 0 end) as Dataset_Creates_Last_120
from `analytics_datasources_all`
Where datediff(CURRENT_DATE(),`created`) < 120
group by `responsible_user_id`

--------------------------------

--Identifying # of datasflows created in last 120 days (4 month period)

Select 
`owner_user_id`
,sum(case when datediff(CURRENT_DATE(),`created`) < 120 then 1 else 0 end) as Dataflow_Creates_Last_120
from `prod_dataflows`
where datediff(CURRENT_DATE(),`created`) < 120
group by `owner_user_id`

--------------------------------

--Identifying # of logins in last 120 days (4 month period)

Select `user id`
,sum(case when datediff(CURRENT_DATE(),`last login`) < 120 then 1 else 0 end) as User_Logins_Last_120
from `analytics_user_list_all`
where datediff(CURRENT_DATE(),`last login`) < 120
group by `user id`

--------------------------------

----Identifying # of cards created in last 120 days (4 month period)

Select 
`creator_user_id`
,sum(case when datediff(CURRENT_DATE(),`created`) < 120 then 1 else 0 end) as Card_Creates_Last_120
from `analytics_card_info_all`
where datediff(CURRENT_DATE(),`created`) < 120
group by `creator_user_id`

--------------------------------

--Adoption opportunities

Select 
`account id`
,sum(case when `type` = 'New Logo' and datediff(CURRENT_DATE(),`Close Date`) <=90 then 1 else 0 end) as Adoption_New_Logo_Last_90
,sum(case when `type` = 'Upsell' and datediff(CURRENT_DATE(),`Close Date`) <=30 then 1 else 0 end) as Adoption_Upsell_Last_30
-- ,`account name`
-- ,`Close Date`
-- ,`Stage`
-- ,`type`
-- ,`TCV Recurring`
from `salesforce_opportunities_master`
where `type` in ('Upsell', 'New Logo')
and `Stage` = 'Closed Won'
and `TCV Recurring` >0
and datediff(CURRENT_DATE(),`Close Date`) <=90
group by 
`account id`
--------------------------------

-- Master Join


Select *

,case when datediff(CURRENT_DATE,`NewUser_last_date`) <= 9999 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0
      when `new_user_intro_candidate` = 1 then 1
      else 0 END
      as flag_new_user

,case when datediff(CURRENT_DATE, `FlyWheel_last_date`) <= 9999 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0
      when `flywheel_candidate` = 1 then 1
      else 0 END
      as flag_Flywheel 

,case when datediff(CURRENT_DATE, `7_Samurai_last_date`) <= 9999 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0
      when `7_samurai_candidate` = 1 then 1
      else 0 END
      as flag_7_Samurai



,case when datediff(CURRENT_DATE, `Escalation_last_date`) <= 120 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0  
      when `escalation_candidate` = 1 then 1 
      else 0 END
      as flag_escalation_content      


,case when datediff(CURRENT_DATE, `personal_health_last_date`) <= 120 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0  
      when `Dataflow_Creates_Last_120` >=1 
      or `Dataset_Creates_Last_120` >=1 
      or `Card_Creates_Last_120` >=1 
      or `User_Logins_Last_120` >=1 
      then 0
      else 1 END
      as flag_personal_health
     
,case when datediff(CURRENT_DATE, `Adoption_last_date`) <= 120 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0
      when `Adoption_New_Logo_Last_90` >=1 then 1
      when `Adoption_Upsell_Last_30` >=1 then 1
      else 0 END
      as flag_adoption          

,case when datediff(CURRENT_DATE, `Instance_Governance_last_date`) <= 90 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0  
      when `stale_card_1` is null then 0
      when `stale_card_2` is null then 0
      when `stale_card_3` is null then 0
      when `stale_card_4` is null then 0
      when `stale_card_5` is null then 0
      when `stale_user_1` is null then 0
      when `stale_user_2` is null then 0
      when `stale_user_3` is null then 0
      when `stale_user_4` is null then 0
      when `stale_user_5` is null then 0
      when `stale_dataset_1` is null then 0
      when `stale_dataset_2` is null then 0
      when `stale_dataset_3` is null then 0
      when `stale_dataset_4` is null then 0
      when `stale_dataset_5` is null then 0
      else 1 END as flag_instance_governance
    
,case when datediff(CURRENT_DATE, `Bi_Annual_Review_last_date`) <= 180 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0  
      when datediff(CURRENT_DATE, `Bi_Annual_Review_last_date`) is null then 1
      else 1 END 
      as Flag_Bi_Annual_Review
 
,case when datediff(CURRENT_DATE, `constant_contact_last_date`) <= 30 then 0
	  when datediff(CURRENT_DATE, `any_acsm_last_date`) < 7 then 0  
      else 1 END
      as flag_constant_contact
 
from `join_em_up`
--------------------------------

--Finally, which Eloqua email is triggered ? 

Select *
, case when `flag_new_user` = 1 then '08.19_ACSM_US_NewUser' 
       when `flag_Flywheel` = 1 then '08.19_ACSM_US_Flywheel'
       when `flag_7_Samurai` = 1 then '08.19_ACSM_US_7_Samurai'
       when `flag_escalation_content` = 1 then '08.19_ACSM_US_Escalation'
       when `flag_personal_health` = 1 then '08.19_ACSM_US_PersonalHealth'
       when `flag_adoption` = 1 then '08.19_ACSM_US_Adoption'
       when `flag_instance_governance` = 1 then '08.19_ACSM_US_Instance_Governance'
       when `Flag_Bi_Annual_Review` = 1 then '08.19_ACSM_US_BiAnnualReview'
       when `flag_constant_contact` = 1 then '07.19_Phase3_ACSM_ConstantContact_1N (Sharing Content)'      
       else 'NoEmail' end as Eloqua_Email_Name
from `email_final_qualification`
where `csm_name` in ('ACSM', 'Guy Welker', 'Maxwell Isley', 'Curtis Philipp', 'Matthew Clarke')
and `email address` not like ('%bigsquid%')
and `email address` is not null
--------------------------------

--------------------------------

--------------------------------

--------------------------------

--------------------------------

--------------------------------

--------------------------------

--------------------------------

--------------------------------
